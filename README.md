![](https://www.cancercentrum.se/globalassets/bilder/logotyper/logotyper-2016/logotyp-samverkan.png)

# Information om R-paket

Detta repository är tänkt att ge kort information samt länkar till andra repositories med R-paket som kan vara relevant för en RCC-statistiker. 
Samtliga av dessa paket kommer att vara installerade på INCAs produktionsmiljö (redan installerade paket markerade med X i INCA-kolumnen nedan). 
För att installera senaste versionen av respektive paket lokalt krävs att R-paketet 'devtools' finns installerat.
Paket installeras sedan med hjälp av:

```
devtools::install_bitbucket("cancercentrum/<PAKET_NAMN>")
```

# Användbara R-paket
| Paket                                               	 | CRAN                                                  | INCA |Beskrivning |                                                                                           
|:-------------------------------------------------------|:------------------------------------------------------|:-----|:---------------------------------------------------------------------------------------|
|[commentr](https://bitbucket.org/cancercentrum/commentr)| [X](https://www.rdocumentation.org/packages/commentr) |      | Funktioner för att underlätta dokumentation av R-kod                                   |
|[decoder](https://bitbucket.org/cancercentrum/decoder)  | [X](https://www.rdocumentation.org/packages/decoder)  | X    | Omkodning från numeriska geografiska/administrativa/medecinska koder till klartext     |  
|[incadata](https://bitbucket.org/cancercentrum/incadata)| [X](https://www.rdocumentation.org/packages/incadata) |      | Identifiering och hantering av dataformat som används av INCA och Rockan               |
|[incavis](https://bitbucket.org/cancercentrum/incavis)  |                                                       | X    | Funktioner för dataexport till 'Vården i siffror'                                      |
|[rccKPL](https://bitbucket.org/cancercentrum/rcckpl)    |                                                       | X    | Funktioner för att skapa 'Koll på läget' i HTML samt PDF                               | 
|[rccmisc](https://bitbucket.org/cancercentrum/rcckpl)   | [X](https://www.rdocumentation.org/packages/rccmisc)  | X    | Blandade funktioner som kan vara av nytta för RCC-data                                 |
|[rccShiny](https://bitbucket.org/cancercentrum/rccShiny)|                                                       |      | Skapa Shiny-appar som komplement till årsrapporter                                     |
